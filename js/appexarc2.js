// Función Generar

/* Criterios para generación de la población
    • Edad valores enteros entre 18 a 99
    • Altura valores reales entre 1.5 a 2.5
    • peso valores reales 20 a 130
*/

var alturaAl = 0;
var pesoAl = 0;

function generar() {

    // Números aleatorios
    let edAleatoria = Math.floor((Math.random() * ((99) - 18)) + 18);
    let alAleatoria = ((Math.random() * ((2.5) - 1.5)) + 1.5).toFixed(1);
    let peAleatorio = ((Math.random() * ((130) - 20)) + 20).toFixed(2);

    alturaAl = alAleatoria;
    pesoAl = peAleatorio;

    // Se obtienen las etiquetas para asignar los valores
    let edad = document.getElementById('txtEdad');
    let altura = document.getElementById('txtAltura');
    let peso = document.getElementById('txtPeso');

    // Impresión de los datos
    edad.innerHTML = edad.setAttribute("value", edAleatoria);
    altura.innerHTML = altura.setAttribute("value", alAleatoria);
    peso.innerHTML = peso.setAttribute("value", peAleatorio);
}

// Función Calcular

/* 
    Se calculará el indice de masa corporal y se mostrará el nivel de peso según la siguiente tabla:

    Menos de 18.5               Bajo peso
    18.5—24.9                   Peso saludable
    25.0—29.9                   Sobrepeso
    30.0 o más                  Obesidad
*/


function calcular() {

    let altura = alturaAl;
    let peso = pesoAl;
    let altura2 = altura*altura;

    // Obtener el IMC
    let resIMC = (peso / altura2).toFixed(1);

    let IMC = document.getElementById('txtIMC');

    IMC.innerHTML = IMC.setAttribute("value", resIMC);

    // Obtener el nivel de peso
    let nivel = document.getElementById('txtNivelP');

    if(resIMC < 18.5){
        nivel.value = "Bajo peso";
    }else if(resIMC>=18.5 && resIMC<=24.9){
        nivel.value = "Peso saludable"
    }else if(resIMC>=25.0 && resIMC<=29.9){
        nivel.value = "Sobrepeso";
    }else if(resIMC>=30.0){
        nivel.value = "Obesidad"
    }
}

// Función registro

var Num = 0;
var prom = 0;
function registro() {
    
    let edad = document.getElementById('txtEdad').value;
    let altura = document.getElementById('txtAltura').value;
    let peso = document.getElementById('txtPeso').value;
    let IMC = document.getElementById('txtIMC').value;
    let nivel = document.getElementById('txtNivelP').value;

    var acumulado = parseFloat(document.getElementById("txtIMC").value);

    Num += 1;
    prom += acumulado;

    document.getElementById('promIMC').innerHTML = (prom/Num).toFixed(2);

    // Jalamos las etiquetas para el ingreso de datos
    let lblNum = document.getElementById('lblNum');
    let lblEdad = document.getElementById('lblEdad');
    let lblAltura = document.getElementById('lblAltura');
    let lblPeso = document.getElementById('lblPeso');
    let lblIMC = document.getElementById('lblIMC');
    let lblNivel = document.getElementById('lblNivel');

    // Mostrar los datos de las etiquetas
    lblNum.innerHTML = lblNum.innerHTML + Num + "<br>";
    lblEdad.innerHTML = lblEdad.innerHTML + edad + "<br>";
    lblAltura.innerHTML = lblAltura.innerHTML + altura + "<br>";
    lblPeso.innerHTML = lblPeso.innerHTML + peso + "<br>";
    lblIMC.innerHTML = lblIMC.innerHTML + IMC + "<br>";
    lblNivel.innerHTML = lblNivel.innerHTML + nivel + "<br>";

}

// Funcion borrar registro

function borrarRegistros(){

    let edad = document.getElementById('txtEdad');
    let altura = document.getElementById('txtAltura');
    let peso = document.getElementById('txtPeso');
    let IMC = document.getElementById('txtIMC');
    let nivel1 = document.getElementById('txtNivelP');

    let lblNum = document.getElementById('lblNum');
    let lblEdad = document.getElementById('lblEdad');
    let lblAltura = document.getElementById('lblAltura');
    let lblPeso = document.getElementById('lblPeso');
    let lblIMC = document.getElementById('lblIMC');
    let lblNivel = document.getElementById('lblNivel');
    let lblProm = document.getElementById('promIMC');
 
    nivel1.value = '';
    edad.innerHTML = edad.setAttribute("value", '');
    altura.innerHTML = altura.setAttribute("value", '');
    peso.innerHTML = peso.setAttribute("value", '');
    IMC.innerHTML = IMC.setAttribute("value", '');
    
    

    lblNum.innerHTML = "";
    lblEdad.innerHTML = "";
    lblAltura.innerHTML = "";
    lblPeso.innerHTML = "";
    lblIMC.innerHTML = "";
    lblNivel.innerHTML = "";
    lblProm.innerHTML = "";
    prom = 0;
    Num = 0;
}